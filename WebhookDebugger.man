NAME
     WebhookDebugger - Debuging Webhooks

SYNOPSIS
     WebhookDebugger -p 443 -a 172.0.0.1

DESCRIPTION
     Simple IPTable

OPTIONS

     -h, --help                                       show this help message and exit
     -p PORT, --port PORT                             set server Port default=8080
     -a ADDR, --addr ADDR                             set server address default 0.0.0.0
     --no-stdout                                      disable log to stdout
     -s STATUS, --status STATUS                       set Http Status Code default=200
     -c CONTENT_TYPE, --content-type  CONTENT_TYPE    set Content-Type default=text/html
     --no-header                                      disables Header output
     --no-body                                        disables Body output
     --no-url                                         disables Url output
     --no-ip                                          disables IP output
     -r RESPONSE, --response RESPONSE                 defines response default same output as STDOUT
     --no-post                                        disables POST output
     --no-get                                         disables GET output
     --ip IP                                          shows only the output for the given ips spaerator=,
     --ssl-cert SSLCERT                               path to ssl cert (LEs fullchain.pem)
     --ssl-key SSLKEY                                 path to ssl key (LEs privkey.pem)
     --sys-version SYS_VERSION                        set Sys Version default=WebhookDebugger/python
     --server-version SERVER_VERSION                  set Server Version default=WebhookDebugger/0.0.3
     --version                                        show program's version number and exit
     forward
          -h, --help                                  show this help message and exit
          --url FORWARD_URL                           set Back-ends URL
          --port FORWARD_PORT                         set Port Back-end is listeing on
          --no-stdout                                 disable log to stdout
          --use-ssl                                   disables forwarding using SSL
          --no-header                                 disables Header output
          --no-body                                   disables Body output
          --no-url                                    disables Url output




AUTHOR
     Lina Linn <WebhookDebugger at apps dot lina dot cloud>